---
title: Site Search
# Site Search

Site search is driven by Acquia Search, using Solr 7. Solr servers
are defined in a post-settings-php factory hook. Connection details
can be found in the Acquia Cloud UI under "Product Keys."

When setting up a new site, the credentials must be entered in
manually at /admin/config/search/acquia-search-solr.
